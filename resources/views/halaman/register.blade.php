<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Form</title>
  </head>

  <body>
  <h1><b>Buat Account Baru!</b></h1>

  <h3>Sign Up Form</h3>

  <form class="" action="/welcome" method="post">
    @csrf
  <label for="fname">First Name:</label>
    <br><br>
    <input type="text" name="fname" value="">
    <br><br>
    <label for="lname">Last Name: </label>
    <br><br>
    <input type="text" name="lname" value="">
    <br><br>
    <label for="gender">Gender:</label>
    <br><br>
    <input type="radio" name="" value=""> Male
    <br>
    <input type="radio" name="" value=""> Female
    <br>
    <input type="radio" name="" value=""> Other
    <br><br>
    <label for="nationality">Nationality</label>
    <br><br>
    <select class="nationality" name="">
      <option value="Indonesian">Indonesian</option>
      <option value="nonIndonesian">Non Indonesian</option>
    </select>
    <br><br>
    <label for="language">Language spoken: </label>
    <br><br>
    <input type="checkbox" name="Indo" value="">Bahasa Indonesia
    <br>
    <input type="checkbox" name="english" value=""> English
    <br>
    <input type="checkbox" name="other" value=""> Other
    <br><br>
    <label for="bio">Bio:</label>
    <br><br>
    <textarea name="bio" rows="8" cols="80"></textarea>
    <br>
    <input type="submit" name="" value="Sign Up" > 
  </form>

  </body>
</html>
