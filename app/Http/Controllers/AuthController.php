<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function data(){
        return view('halaman.register');
    }

    public function selamatdatang(Request $request){
        
        $nama1 = $request['fname'];
        $nama2 = $request['lname'];
         
        return view('halaman.welcome', compact('nama1', 'nama2') );
    }
}
