<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\HomeController@text');
Route::get('/register', 'App\Http\Controllers\AuthController@data');
Route::post('/welcome', 'App\Http\Controllers\AuthController@selamatdatang');

Route:: get('/master',function(){
    return view('lyout.master');
});
Route:: get('/table',function(){
    return view('halaman.table');
});

Route:: get('/datatable',function(){
    return view('halaman.datatable');
});
